const Sequelize = require('sequelize');
const db = require('../util/db');

const Programmer = db.define(
    "programmer",
    {
        name:{type:Sequelize.STRING},
        city:{type:Sequelize.STRING},
        lang:{type:Sequelize.STRING},
        experience:{type:Sequelize.STRING},
        gender:{type:Sequelize.STRING},
    }
);

Programmer.sync({});

module.exports = Programmer;