const db = require('../util/db');
const User = require('../models/user');
const uuid = require('uuid').v4
const session = require('express-session');

const getUser = async obj => {
    return await User.findOne({
        where:obj
    });
}

exports.storeRegister = async (req,res) => {
    console.log("action register");
    try {
        console.log(req.body);
        const {name,email,password} = req.body;

        const registerUser = new User({
            name,email,password
        });

        await registerUser.save((result,err) => {
            if(err){
                console.error(err);
            }
            console.log(result);
        });

        res.redirect('/login');
    } catch (err) {
        console.error(err.message);
        res.status(500).send('server error');
    }
}

exports.actionLogin =async (req,res,next) => {
    console.log("login action");
    try {
        console.log(req.body);
        const {email,password} = req.body;

        if(email && password){
            let user = await getUser({email:email});
            
            if(!user){
                console.log("email not found");
                res.redirect('/login');
            }

            if(user.password === password){
                const uniqueId = uuid();
                console.log("success Login ",uniqueId);
                req.session.loggedin = true;
				req.session.loggedinId = uniqueId;
				req.session.loggedName = user.name;

                res.redirect('/dashboard');
                // next();
            }else{
                console.log("email & password not match");
                res.redirect('/login');
            }
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send('server error');
    }
}

exports.actionLogout = (req,res) => {
    console.log("logout action");
    req.session.destroy();
    res.redirect('/login');
}
