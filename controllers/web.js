module.exports.login = async (req,res,next) => {
    if(req.session.loggedin){
        return res.redirect('/');
    }
    res.render('login',{
        pageTitle: 'Add Product',
        path: 'login',
        editing: false
    });
}

module.exports.register = async (req,res,next) => {
    if(req.session.loggedin){
        return res.redirect('/');
    }
    res.render('register',{
        pageTitle: 'Register',
        path: 'register',
        editing: false
    });
}