const Programmer = require('../models/programmer');

module.exports.home = async (req,res) => {
    res.send("home page");
}
module.exports.dashboard = async (req,res) => {
    const progLang = ['PHP','Javascript','iOs','NET','GoLang','Ruby'];
    const gender = ['Female','Male'];

    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({
        where : whereClause
    })
    .then(programmers => {
        const label = [];
        const total = [];
        const prg = JSON.parse(JSON.stringify(programmers));
        
        res.render('dashboard',{
            pageTitle: 'Dashboard',
            progLang : progLang,
            gender : gender,
            programmers : prg,
            loggedName: req.session.loggedName,
            path: 'dashboard',
            editing: false
        });
    })
    .catch(err => {
      console.log(err);
    });
}