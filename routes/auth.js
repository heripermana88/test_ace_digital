const express = require('express');

const authController = require('../controllers/auth');

const router = express.Router();

router.post('/register', authController.storeRegister);

router.post('/login', authController.actionLogin);

router.get('/logout', authController.actionLogout);

module.exports = router;