const express = require('express');
const sequelize = require('sequelize');

const adminController = require('../controllers/admin');
const Programmer = require('../models/programmer');

const router = express.Router();

router.use('/dashboard',(req,res,next) => {

    if(!req.session.loggedin){
        return res.redirect('/login');
    }
    next();
});

router.get('/dashboard', adminController.dashboard);

router.get('/renderProgrammer', (req,res) => {
    const sex = ['f','m'];
    const lang = ['PHP','Javascript','iOs','NET','GoLang','Ruby'];
    const city = ['Jakarta','Bandung','Medan','Surabaya','Cirebon','Djogja'];
    for (let index = 0; index < 1500; index++) {
        const exp = Math.round(Math.random() * (4 - 1) + 1);
        const randLang = Math.round(Math.random() * (5 - 1) + 0);
        const randCity = Math.round(Math.random() * (5 - 1) + 0);
        const gender = sex[Math.round(Math.random() * (1 - 0) + 0)];
        Programmer.create({ name: 'Programmer - '+index, city: city[randCity], lang:lang[randLang], experience: exp, gender: gender});
    }
    res.send('success render');
});

router.get('/jsonData', (req,res) => {
    res.json({data:[12, 19, 50, 5, 2, 3]});
});

router.get('/jsonProgrammers', (req,res) => {
    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({where : whereClause})
    .then(programmers => {
      res.json({
        data: programmers,
      });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/byGender', (req,res) => {
    Programmer.findAll({
        attributes :[
            'gender',
            [sequelize.fn('COUNT', sequelize.col('gender')), 'total']
        ],
        group: 'gender'
    })
    .then(programmers => {
      res.json({
        data: programmers,
      });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/byLang', (req,res) => {
    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({
        where : whereClause,
        attributes :[
            'lang',
            [sequelize.fn('COUNT', sequelize.col('lang')), 'total']
        ],
        group: 'lang',
        order: [
            ['lang', 'ASC']
        ]
    })
    .then(programmers => {
        const lang = [];
        const total = [];
        const prg = JSON.parse(JSON.stringify(programmers));
        for(var i in prg){
            lang.push(prg[i].lang);
            total.push(prg[i].total);
        }

        res.json({
            lang,total
        });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/byCity', (req,res) => {
    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({
        where : whereClause,
        attributes :[
            'city',
            [sequelize.fn('COUNT', sequelize.col('city')), 'total']
        ],
        group: 'city'
    })
    .then(programmers => {
        const city = [];
        const total = [];
        const prg = JSON.parse(JSON.stringify(programmers));
        for(var i in prg){
            city.push(prg[i].city);
            total.push(prg[i].total);
        }

        res.json({
            city,total
        });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/byExp', (req,res) => {
    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({
        where : whereClause,
        attributes :[
            'experience',
            [sequelize.fn('COUNT', sequelize.col('experience')), 'total']
        ],
        group: 'experience'
    })
    .then(programmers => {
        const label = [];
        const total = [];
        const prg = JSON.parse(JSON.stringify(programmers));
        for(var i in prg){
            label.push(prg[i].experience+' Year');
            total.push(prg[i].total);
        }

        res.json({
            label,total
        });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/tabular', (req,res) => {
    console.log(req.query)
    const whereClause=req.query;

    Programmer.findAll({
        where : whereClause
    })
    .then(programmers => {
        const label = [];
        const total = [];
        const prg = JSON.parse(JSON.stringify(programmers));
        // for(var i in prg){
        //     label.push(prg[i].city);
        //     total.push(prg[i].total);
        // }

        res.json({
            prg
        });
    })
    .catch(err => {
      console.log(err);
    });
});

module.exports = router;