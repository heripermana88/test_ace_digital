const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const port = 3000;

app.set('view engine','ejs');
app.set('views','views');

const authRoutes = require('./routes/auth'); 
const adminRoutes = require('./routes/admin'); 
const webController = require('./controllers/web')

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname,'public')));

app.use('/auth',authRoutes);
app.use(adminRoutes);
app.use('/login',webController.login);
app.use('/register',webController.register);

app.use('/',(req,res) =>{
    res.redirect('/dashboard');
});

app.listen(port,()=>console.log("node js runing on http://localhost:"+port));